\documentclass{article}
\usepackage{color}
\usepackage{hyperref}
\usepackage{graphicx}
\begin{document}

	\begin{center}
		PHYS 689: Methods of Experimental Particle Physics. Fall 2015. \\ Lab Assignment 3\\
	\end{center}

	\begin{center}

		{\bf Fitting Distributions and Understanding the Results}\\
	\end{center}

	
	\vskip6mm
		
	\begin{center}
		Links and References:
	\end{center}
	
	\begin{itemize}
		\item User's Guide: \href{http://root.cern.ch/drupal/content/users-guide}{http://root.cern.ch/drupal/content/users-guide}
		\begin{itemize}
			\item Fitting histograms: \href{http://root.cern.ch/download/doc/5FittingHistograms.pdf}{http://root.cern.ch/download/doc/5FittingHistograms.pdf}
		\end{itemize}
		\item Reference Guide: \href{http://root.cern.ch/root/html534/ClassIndex.html}{http://root.cern.ch/root/html534/ClassIndex.html}
		\begin{itemize}
			\item TH1F: \href{http://root.cern.ch/root/html534/TTree}{http://root.cern.ch/root/html534/TH1F}
			\item TF1: \href{http://root.cern.ch/root/html534/TCut}{http://root.cern.ch/root/html534/TF1}
		\end{itemize}
	\end{itemize}
	
	\vskip6mm	
	
		
\section{Parameter Estimation and Statistical Tests}

Fitting an analytical function to data to finding the parameters of the function is a fairly typical exercise in data analysis. In  many cases it is a fairly straightforward procedure with very clear meaning, e.g. if you are looking at a distribution of the  invariant mass of a pair of muons in data events and you see a resonance (a gaussian-looking peak), fitting a gaussian function to the data would allow you to calculate the normalization of the signal (which you can later convert into the cross-section measurement), the width of the distribution is related to the resonance  lifetime and the mean is the mass of the resonance. In the following we discuss the machinery usually used to perform parameter estimation.

The ``best parameters'' however are not necessarily ``good''. When in the previous lab assignment you were fitting a distribution in data to an analytic function with the intent to extract the parameters of the function that describe the data, you were implicitly assuming that the data follows the function that you used. That's not a given however and that  assumption  may or may not be true. In an extreme case, one can take a function that doesn't look much like the data at all and still the fit will yield some ``best parameter values''. In reality the function with these ``best parameters'' may not resemble the data at all, it's just that other values make the agreement even worse. ``Hypothesis testing'' and the``goodness of fit'' determination are statistical procedures that allow one answer the question of whether the best function you obtained is good or not.

\subsection{Parameter Estimation}

In the previous lab assignment you fitted data (represented as integer-valued entries counting ``events'' in bins of a histogram) using $\chi^{2}$ and likelihood methods. The former uses the $\chi^{2}$ metrics:

\begin{eqnarray}
\chi^2( \vec{p} ) =  \sum_{i=1}^{N_{bin}}( f (\vec{p}, x_i)-   D_i)^2 / \sigma_i^2),
\end{eqnarray}
where $x$ is the variable used as the $x$-axis of the histogram you are fitting, $x_i$ is the center of bin $i$, $f$ is the function you are fitting to data that depends on $x$ and the vector of parameters $\vec{p}$, $D_i$ is the measurement (the number of events in data) in bin $i$. For each bin, the numerator of this expression can be though as the difference between the number of observed events and the expectation given by the value of $f$ for each set of parameters $\vec{p}$. The latter however is measured in absolute number of events, which is not very meaningful. For example, a difference of 5 events in the bin where $f(x_i)=1005$ and $D_i=1000$ would seem as a good agreement while the same difference in the bin where the expectation $f_i=0.0000001$ and $D_i=5$ would seem like a huge discrepancy. Therefore, in the $\chi^2$ statistic this absolute difference is scaled by the statistical variance $\sigma_i$ representing the expected spread in the number of observed events when the expected number of events is given by $f(x_i)$. In the example we have been discussing, the data is following a Poisson distribution, so the variance $\sigma_i=\sqrt{f_i}$. Finding the set of most likely parameters $\vec{p}$ of function $f$ is equivalent to finding such set of parameters where the $\chi^2$ reaches its minimum. 

In the other case, the fit procedure is equivalent to finding the maximum of a likelihood function defined as:
\begin{eqnarray}
{\cal L} (\vec{p}) =  \prod_{i=1}^{N_{bins}}{P(f(x_i, \vec{p}), D_i)},
\end{eqnarray}
where $P (\nu, N) = \nu^N e^{-\nu}/N!$ is the Poisson distribution. The meaning of the function in the product is fairly simple: for each bin $i$, it is the probability to observe $D_i$ events when the expected number of events is $f(x_i, \vec{p})$. 

Once you multiply the probabilities for each bin, you obtain the probability to observe the exact numbers of events in each bin $i$ as observed in data. If you think of it, with even a moderate number of bins, the value of ${\cal L}$ must be very small even if the function and data look very much alike. Think of it this way: while seeing 20 events in data while the expectation is 23.0 seems quite plausible, the actual probability to observe exactly 20 events (not 21, not 24, but exactly 20) is only about 5\%. If your data histogram has 10 bins and the probability in each is about 10\%, the final likelihood value will be $\sim (0.1)^{10}$, i.e. an incredibly small number. It should not come as a surprise as this is not a measure of how plausible the observed outcome is, but rather the probability to observe ``this exact distribution of events given the expectation in each bin'', and the two are very different questions. 

The most probable set of parameters $\vec{p}$ is obtained as the values that correspond to the minimum of the likelihood function. When performing numeric computations, one should keep in mind that coding the calculation of ${\cal L}$ as a straight  product of many small numbers will likely lead to severe instabilities due to limited machine accuracy. One typical trick to avoid that would be to sum logarithms of probabilities:
\begin{eqnarray}
\mathrm{log} {\cal L} =  \mathrm{log}\prod_{i=1}^{N_{bins}}{P(f_i, D_i)} = \sum_{i=1}^{N_{bins}}{\mathrm{log}P(f_i, D_i)}.
\end{eqnarray}
and then exponentiate the sum to obtain the value of {\cal L}. This last step is usually not needed as both ${\cal L}$ and $\mathrm{log}{\cal L}$ achieve their maximum at the same point, and hence $\mathrm{log}{\cal L}$ can be used instead of ${\cal L}$.

If the expected number of events in each bin is large, either the $\chi^2$ or likelihood method yield similar results. One limitation of the $\chi^2$ method is that when the expected number of events in some of the bins is small (say less than 10), the simplification associated with using a simple $\sqrt{f}$ in the denominator can bias the results. The likelihood method, which explicitly calculates Poisson probabilities, becomes the preferred method. 

\subsection{Uncertainties in Parameter Estimation}

While the ``best'' parameters are determined by finding the maximum of the likelihood function (or minimizing $\chi^2$), small deviations of the parameters from the minimum will yield functions that are almost as good, which brings the question of how to quantify the uncertainties in parameter determination. 

For simplicity let's assume that we are fitting a function $f(x)=p$ to data that appears as a flattish looking distribution of the number of events in several bins from $x=0$ to $x=1$ for only one parameter, the normalization. As the fit procedure assumes that the functional form of the distribution is known, at least one value of $p$ must be the correct value. The likelihood function ${\cal L}(p)$ gives the (differential) probability of different values of $p$ being true, and as at least one value got to be true, the integral of {\cal L} over all possible values of $p$ must be equal to one. Some values of $p$ have higher probability than others, so one can define the region (interval) of values for parameter $p$ in which the likelihood function accumulates 68\% of its full integral. As the full integral is equal to one (at least one value of $p$ is correct), this region is said to be the 68\% confidence level interval for the values of parameter $p$. Note that there is no unique way to define such region, a usual convention is to report the highest posterior density interval, which is determined as a region which has equal differential probabilities on the left and on the right and contains the maximum of the distribution. 

In the case of two parameters, one would plot the likelihood function in two dimensions and determine the 2-dimensional shape which contains 68\% of the integral with equal probabilities (values of ${\cal L}$) on the edges of the region and the maximum being within the interval. Such 2-dimensional regions would typically look like ellipses or circles. The shape is determined by the correlation of the parameters with each other (e.g. sometimes one can make the function better resemble the data by moving one or the other parameter getting about the same amount of improvement or one can simultaneously change two parameters in some specific, correlated, way with small or no change to the final likelihood function value).

There is of course no magic in the value of 68\%, it's just a convention and one could use a different confidence level. 68\% corresponds to the integral under a gaussian distribution within $\pm 1 \sigma$ of it's maximum. So 68\% C.L. is also often referred to as ``one sigma'', 95\% corresponds to ``two sigma'' etc.

In some cases, one chooses to report one-sided intervals. A typical example would be an experiment in which you measure a cross-section by looking at a distribution of data, which you assume to be a sum of signal and background of known shapes. You want to determine how many events contribute to the signal and that would be what you need to calculate  the cross-section. To do that, you will fit a function $f(x)= N_s \times D_s(x)+ N_b D_b(x)$ to the data, where $D_s$ and $D_b$ are the known normalized (to a unity integral) distributions for signal and background processes and parameters $N_s$ and $N_b$ are the ``rates'' of signal and background events. For simplicity let's say $N_b$ is known and is an exact number, so you are fitting for just a single parameter $N_s$. Now imagine that the data shows no clear signs of ``signal'' (no bump or maybe a very small bump) and when you plot the likelihood versus $N_s$, the value of $N_s=0$ appears very probable. In cases like this, one often quotes the ``upper limit'' on possible $N_s$ values. In this case one would find the value $N_s^{95}$ such that the integral of {\cal L} above it (up until infinity) is 5\% of the full integral. The meaning of this would be that the probability that the true number of expected signal events is above $N_s^{95}$ is only 5\%. Note that in this example, one may choose to set the likelihood function to be zero for $N_s<0$ arguing that the region of negative values is non-physical (a signal can be there or not be there, but it can't have a negative cross-section). in this case one will have to rescale {\cal L} to ensure that the integral over $N_s$ from zero to infinity is a unity (instead of the integral from negative infinity to positive infinity being a unity). Depending on whether you choose to consider the region of negative cross-section allowed or disallowed, the limit will obviously be different. Including such ``external knowledge''  is effectively conditioning the outcome on one's personal subjective``prior belief'' and is a commonplace in Bayesian statistics.


\subsection{Hypothesis Testing}

As we discussed earlier, one can always fit a function to data and obtain the set of most probable parameters along with the corresponding uncertainty intervals, it is entirely possible that the function with the most likely choice of parameters may look nothing like data. Of course, an extreme discrepancy would indicate that you made a poor choice of a function, but when the discrepancy is not enormous, one needs to find a way to quantify the level of agreement between the data and the function. One way to do that would be to form a hypothesis (say that the function with the best parameters will be your hypothesis of the true distribution that the data follows) and then test this hypothesis against the data. 

While there are various simple indicators one can rely upon (for example, one can use the value of $\chi^2$ at the position of the minimum to quantify the probability that the observed data follows the distribution determined by your hypothesis), one of the most common tests is the so called p-value. The p-value is defined as the probability that, under the assumption that the data truly follows the selected hypothesis, a hypothetical experiment could yield  an observation of the data that is less consistent with the hypothesis than the one obtained in the actual measurement. One of course needs to define what is ``likely'' or ``not likely'' means first using a suitable figure of merit (statistic). One example of a statistic choice would be the likelihood function {\cal L} where one compares a function (without parameters, e.g. if you define your hypothesis to correspond to the function with the parameters at the maximum of the likelihood function) and the data. If the value of {\cal L} for a particular ``outcome'' is less than the one observed in the actual data, such ``outcome'' is less similar to the hypothesis than the one observed in data. 

Calculation of the p-value would typically involve pseudo-experiments. One would start with a hypothesis (think of it as a function), then use Monte Carlo technique to ``generate'' a set of pseudo-data (this is just like when we were generating points according to some distribution when we learned the Monte Carlo integration technique). Every time such pseudo-data is generated, one would calculate the likelihood value and determine whether it's larger or smaller than the one observed in actual data. If after generating thousands of pseudo-data sets, you determine that only 1\% of the pseudo-experiments results in the ``outcomes'' less likely than the one observed in data, you would say that the p-value is 1\% and it would be an indication that something may be going wrong and perhaps the function you obtained is not very likely to be what the data follows. 

There are ``standard'' thresholds that have been adopted in particle physics to rule out a hypothesis, e.g. the excess of Higgs-like events at the LHC has been proclaimed as a discovery after the p-value that quantifies the level of agreement between the data and the Standard Model without the Higgs boson has become smaller than ``5 sigma'' or p-value less than 0.0000003.

\section{Lab Assignment}
As always, you will need to produce your report as a LaTex '``article'' with the required plots, figures and discussions documenting your research.

\subsection{Part 1: Parameter Evaluation and Limit Setting}
Use the code you wrote in the previous lab assignments to fit a histogram shown in Fig.~\ref{fig1} using the following functional form: $f(m) = B \times exp{-\alpha m} + S \cdot exp(\frac{-(m-m_0)^2}{(2\sigma^2)})$

The meaning of the parameters here is the normalizations for ``signal'' and ``background'' distributions. In this case you are hunting for a possible ``bump'' in the mass spectrum. Parameters, $\alpha$, $m_0$ and $\sigma$ are known, so you have two fit parameters. Perform a fit using maximum likelihood method (using your own code), find the values of $S$ and $B$ that minimize the likelihood. Draw contours that correspond to one and two sigma levels on a graph. Discuss your results including possible correlations.

\begin{figure}
\begin{center}
\includegraphics[width=0.8\linewidth]{m-j-psi.pdf} 
\end{center}
\caption{The distribution of the invariant mass of two muons in a hypothetical experiment in which you are searching for a $J/\psi$ resonance. \label{fig1}}
\end{figure}

Next, calculate the 95\% C.L. upper limit for the normalization of the signal, including a physical consideration that the rate of signal can be either positive or zero, but not negative (here, you effectively including a Bayesian prior). Discuss how the calculation is different from the 2 sigma contours above.

\subsection{Part 2: Hypothesis Testing for SImple Examples}

We will start with two similar examples, in which we will fit two histograms using functional form $f(x)=A$. The histograms are shown in Fig.~\ref{fig2} and there is something strikingly different between them. Fit each of the distributions for a constant (does not have to be the same constant). Next, test how likely it is that this data is described by the proposed functional using the p-value test. 

\begin{figure}
\begin{center}
\includegraphics[width=0.8\linewidth]{two-histograms.pdf} 
\end{center}
\caption{The distribution of the invariant mass of two muons in a hypothetical experiment in which you are searching for a $J/\psi$ resonance. \label{fig1}}
\end{figure}

To calculate the p-value for each distribution, you will need to generate pseudo-data using the functional form you obtained that corresponds to the minimum of the likelihood function. Use the likelihood function as the metrics. You will want to plot the likelihood values for each of your pseudoexperiments on a histogram and see where the value of the likelihood for the data is sitting relative to those.

Repeat the same test using $\chi^2$ fit and using $\chi^2 $ as your test statistics instead of the likelihood. What conclusion can you made about these two distributions? Is there something particularly strange or striking about one of them? Please discuss your results and what they are telling you. You will also want to check our $\chi^2$ values against a particular plot in the PDG.

\subsection{Part 3: Hypothesis Testing for a Realistic Example}

Use the ``bump hunting'' distribution to evaluate whether it is likely that the data follows the shape you obtained using the fit. You will obviously need to calculate the p-value.

In addition, check if the data is consistent with the null hypothesis. IN this case, in the formula you obtained after the fit, you will want to set $S$ to zero (no signal). Repeat the p-value calculation. Discuss your results in both cases. 



\end{document}