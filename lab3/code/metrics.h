#include <iostream>
#include <stdlib.h>
#include <time.h>  
#include <math.h>

using namespace std;

Double_t calc_chi2(TH1F* data_hist, TF1*  func,
                   const Double_t data_start = 1,
                   const Double_t data_stop  = 40, // this isn't really needed...
                   const Double_t bin_width  = 1.,
                   const Int_t    num_bins   = 40){

    Double_t chi2 = 0.;

    for(int i = 1; i <= num_bins ; ++i){
        if(func( data_start + ((i-0.5)*bin_width) ) <= 0){
            cout << "function less than or equal to 0, cannot divide by 0. exiting." << endl;
            exit(-1);
        }
        chi2 += pow(func( data_start + ((i+0.5)*bin_width) ) - (data_hist -> GetBinContent(i)),2) / ( 2 * func( data_start + ((i+0.5)*bin_width) ) );
        // divide 2sigma^2 with sigma=sqrt(f)
    }

    if(chi2 == chi2 && chi2 < 9999999.) return chi2;
    else{ cout << "error in calculating chi squared." << endl; exit(-1); }
}

Double_t calc_likelihood(TH1F* data_hist, TF1*  func,
                         const Int_t data_start = 1,
                         const Double_t data_stop  = 40, // this isn't really needed...
                         const Double_t bin_width  = 1.,
                         const Int_t    num_bins   = 40){

    Double_t loglikelihood = 0.;

    for(int i = 1; i <= num_bins; ++i){
        if(func( data_start + ((i-0.5)*bin_width) ) <= 0){
            cout << "function less than or equal to 0, cannot call Poisson of such a number. exiting." << endl;
            exit(-1);
        }
        loglikelihood -= func( data_start + ((i-0.5)*bin_width));
        loglikelihood += data_hist -> GetBinContent(i)*TMath::Log(func( data_start + ((i-0.5)*bin_width)));
        loglikelihood -= TMath::Log(data_hist -> GetBinContent(i));
// This was giving wrong results when data was = 0 ... I don't think that should happen ...
        //loglikelihood += TMath::Log(TMath::Poisson(func( data_start + ((i-0.5)*bin_width) ), data_hist -> GetBinContent(i)));

        /*cout << "bin " << i << ". func at " << data_start + ((i-0.5)*bin_width) << ": "
             << func(data_start + ((i-0.5)*bin_width) ) << " (taking Log of Poisson of this number)"
             << ". data at this bin: " << data_hist -> GetBinContent(i) << endl
             << "running loglikelihood: " << loglikelihood << endl;*/
    }
    loglikelihood = -1.*loglikelihood;

    if(loglikelihood == loglikelihood && loglikelihood < 9999999.) return loglikelihood;
    else{ cout << "error in calculating negative log likelihood." << endl; exit(-1); }
}

// Find the error ranges given the function, the minimum index, and the deviation
void find_error(Double_t *func_vals, const Int_t length, const Double_t min_val,
                const Double_t deviation, const Int_t val_index,
                Int_t &left_error, Int_t &right_error){

    Int_t index_itr = val_index + 1;
    while(index_itr < length && func_vals[index_itr] - min_val < deviation)
        ++index_itr;
    right_error = index_itr;

    index_itr = val_index - 1;
    while(index_itr > 0 && func_vals[index_itr] - min_val < deviation)
        --index_itr;
    left_error = index_itr;
}

// Find the error ranges for the chisquared and likelihood functions specifically 
void find_error_chi2likeli(TH1F* data_hist, TF1* func,
                           const Double_t min_param_val,
                           Double_t &left_error, Double_t &right_error,
                           Double_t start, Double_t stop,
                           const Double_t step,
                           const Double_t deviation,
                           const Int_t param1_flag  = 1,
                           const string metric_flag = "chi2"){ // just do both at same time?

    if(metric_flag != "chi2" && metric_flag != "likeli"){
        cout << "Not a valid metric choice to minimize." << endl;
        exit(-1);
    }

    Double_t func_val, min_metric_val;
    func -> SetParameter(param1_flag,min_param_val);
    if(metric_flag == "chi2") min_metric_val = calc_chi2(data_hist, func);
    else min_metric_val = calc_likelihood(data_hist, func);

    Double_t param_val = min_param_val + step;
    while(param_val <= stop){
        func -> SetParameter(param1_flag,param_val);
        if(metric_flag == "chi2"){
            func_val = calc_chi2(data_hist, func);
        }else{
            func_val = calc_likelihood(data_hist, func);
        }
        if(func_val - min_metric_val >= deviation) break;
        param_val += step;
    }
    right_error = param_val - min_param_val;

    param_val = min_param_val - step;
    while(param_val >= start){
        func -> SetParameter(param1_flag,param_val);
        if(metric_flag == "chi2"){
            func_val = calc_chi2(data_hist, func);
        }else{
            func_val = calc_likelihood(data_hist, func);
        }
        if(func_val - min_metric_val >= deviation) break;
        param_val -= step;
    }
    left_error = min_param_val - param_val;
}

// Find the 2-D error ranges given the function, the minimum index, and the deviation
void find_error2D(Double_t *func_vals, const Int_t length1, const Int_t length2,
                  const Double_t min_val, const Double_t deviation,
                  const Int_t val_index1, const Int_t val_index2,
                  Int_t &left_error1, Int_t &right_error1,
                  Int_t &left_error2, Int_t &right_error2){

    Int_t index_itr = val_index1 + 1;
    while(func_vals[val_index2 + (length2*index_itr)] - min_val < deviation) ++index_itr;    
    right_error1 = index_itr;

    index_itr = val_index1 - 1;
    while(func_vals[val_index2 + (length2*index_itr)] - min_val < deviation) --index_itr;    
    left_error1 = index_itr;

    index_itr = val_index2 + 1;
    while(func_vals[index_itr + (length2*val_index1)] - min_val < deviation) ++index_itr;    
    right_error2 = index_itr;

    index_itr = val_index2 - 1;
    while(func_vals[index_itr + (length2*val_index1)] - min_val < deviation) --index_itr;    
    left_error2 = index_itr;
}

// Find error on chi2 and likelihood distribution parameter1 to the right and left
void find_errorchilikeli2D(){

    Int_t index1_itr = min_chi_1index + 1;
    Int_t index2_itr = min_chi_2index;
    while(chi2_vals[index2_itr + (num2_samples*index1_itr)] - min_chi < chi_deviation){
        ++index1_itr;
    }
    chi_1right_error = param1_vals[index1_itr] - min_param1_chi;

    index1_itr = min_chi_1index - 1;
    while(index1_itr > 0 && chi2_vals[index2_itr + (num2_samples*index1_itr)] - min_chi < chi_deviation){
        --index1_itr;
    }
    chi_1left_error = min_param1_chi - param1_vals[index1_itr];

    index1_itr = min_likeli_1index + 1;
    index2_itr = min_likeli_2index;
    while(likelihood_vals[index2_itr + (num2_samples*index1_itr)] - min_likelihood < likeli_deviation){
        ++index1_itr;
    }
    likeli_1right_error = param1_vals[index1_itr] - min_param1_likeli;

    index1_itr = min_likeli_1index - 1;
    while(index1_itr > 0 && likelihood_vals[index2_itr + (num2_samples*index1_itr)] - min_likelihood < likeli_deviation){
        --index1_itr;
    }
    likeli_1left_error = min_param1_likeli - param1_vals[index1_itr];
// Find error on chi2 and likelihood distribution parameter2 to the right and left
    index1_itr = min_chi_1index;
    index2_itr = min_chi_2index + 1;
    while(chi2_vals[index2_itr + (num2_samples*index1_itr)] - min_chi < chi_deviation){
        ++index2_itr;
    }
    chi_2right_error = param2_vals[index2_itr] - min_param2_chi;

    index2_itr = min_chi_2index - 1;
    while(index2_itr > 0 && chi2_vals[index2_itr + (num2_samples*index1_itr)] - min_chi < chi_deviation){
        --index2_itr;
    }
    chi_2left_error = min_param2_chi - param2_vals[index2_itr];

    index1_itr = min_likeli_1index;
    index2_itr = min_likeli_2index + 1;
    while(likelihood_vals[index2_itr + (num2_samples*index1_itr)] - min_likelihood < likeli_deviation){
        ++index2_itr;
    }
    likeli_2right_error = param2_vals[index2_itr] - min_param2_likeli;

    index2_itr = min_likeli_2index - 1;
    while(index2_itr > 0 && likelihood_vals[index2_itr + (num2_samples*index1_itr)] - min_likelihood < likeli_deviation){
        --index2_itr;
    }
    likeli_2left_error = min_param2_likeli - param2_vals[index2_itr];
}


// Find min of func in given range - assuming monotonic decreasing behavior around minimum within range limits
Double_t find_min_1D(TF1* func, const Double_t step_size,
                     Double_t start, Double_t stop,
                     Double_t precision = 0.01){

    srand(time(0));
    delete gRandom;
    TRandom3 *randnum     = new TRandom3(0);
    Double_t *metric_vals = new Double_t[3];
    Double_t min_val      = 999999.;
    Double_t rand_val     = 0.;

// Begin infinite loop finding min
    while(1){
// Calculate a random parameter value in the range allowed, with precision to two decimal places
        rand_val = ceil(randnum->Uniform(start+step_size,stop-step_size)*100.)/100.;

// Set function and calculate chi squared or likelihood below, at, and above the random parameter choice
        rand_val -= step_size;
        for(int i = 0; i < 3; ++i){
            metric_vals[i] = func(rand_val);
            rand_val += step_size;
        }// I think this is not as efficient as just writing all 6 lines out explicitly...

// Find which direction is decreasing (or if we're at the minimum) and change range values accordingly
        if(metric_vals[0] > metric_vals[1]){ // we're on the left of the minimum
            if(metric_vals[2] > metric_vals[1]){ //check if we found the actual minimum
                min_val = func(rand_val);
                break;
            }else if(rand_val == stop-step_size){ // the minimum happens to be the endpoint of the range we're looking at
                min_val = func(stop);
                break;
            }else start = rand_val; // we're on the left and haven't hit the minimum yet
        }else if(rand_val == start+step_size){ // the minimum happens to be the endpoint of the range we're looking at
            min_val = func(start);
            break;
        }else stop = rand_val; // we must be on the right of the minimum, no other options
// if we haven't found the actual minimum, we need to stop when we are straddling the middle point
        if(stop - start <= 2*step_size){
            min_val = func(start + step_size);
            break;
        }
    }

    delete[] metric_vals;
    delete randnum;

    return min_val;
}


// Same function but specific to chi and likelihood to save computation time so that you don't
// have to calculate all values of chi squared and likelihood before finding the minimum
Double_t find_min_1D_chilikeli(TH1F* data_hist, TF1* func, // data and fitting function
                               const Double_t step, // step size which we're looking through parameter range with
                               Double_t start, Double_t stop, // parameter range
                               const Int_t param1_flag  = 1, // which paramter we're floating
                               const string metric_flag = "chi2", // which metric we're minimizing - change to just do both at once?
                               Double_t precision       = 0.01){

    if(metric_flag != "chi2" && metric_flag != "likeli"){
        cout << "Not a valid metric choice to minimize." << endl;
        exit(-1);
    }

    srand(time(0));
    delete gRandom;
    TRandom3 *randnum     = new TRandom3(0);
    Double_t *metric_vals = new Double_t[3];
    Double_t min_param    = 999999.;
    Double_t rand_param   = 0.;

// Begin infinite loop finding min
    while(1){
// Calculate a random parameter value in the range allowed, with precision to two decimal places
        rand_param = ceil(randnum->Uniform(start+step,stop-step)*1000.)/1000.;

// Set function and calculate chi squared or likelihood below, at, and above the random parameter choice
        rand_param -= step;
        for(int i = 0; i < 3; ++i){
            func -> SetParameter(param1_flag, rand_param);
            if(metric_flag == "chi2") metric_vals[i] = calc_chi2(data_hist, func);
            else                      metric_vals[i] = calc_likelihood(data_hist, func);
            rand_param += step;
        }

// Find which direction is decreasing (or if we're at the minimum) and change range values accordingly
        if(metric_vals[0] > metric_vals[1]){ // we're on the left of the minimum
            if(metric_vals[2] > metric_vals[1]){ //check if we found the actual minimum
                min_param = rand_param;
                break;
            }else if(rand_param == stop-step){ // the minimum happens to be the endpoint of the range we're looking at
                min_param = stop;
                break;
            }else start = rand_param; // we're on the left and haven't hit the minimum yet
        }else if(rand_param == start+step){ // the minimum happens to be the endpoint of the range we're looking at
            min_param = start;
            break;
        }else stop = rand_param; // we must be on the right of the minimum, no other options
// if we haven't found the actual minimum, we need to stop when we are straddling the middle point
        if(stop - start <= 2*step){
            min_param = start + step;
            break;
        }
    }

    delete[] metric_vals;
    delete randnum;

    return min_param;
}

// 2D minimization of chisquared and log likelihood specifically to save computation time so that you don't
// have to calculate all values of chi squared and likelihood before finding the minimum
Double_t find_min_2D_chilikeli(TH1F* data_hist, TF1* func, // data and fitting function
                               const Double_t data_start = 1,
                               const Double_t data_stop  = 40, // this isn't really needed...
                               const Double_t bin_width  = 1.,
                               const Int_t    num_bins   = 40,
                               const Double_t step, // step size which we're looking through parameter range with
                               Double_t start1, Double_t stop1, // parameter range
                               Double_t start2, Double_t stop2, // parameter range
                               const Int_t param1_flag  = 0, // which paramter we're floating
                               const Int_t param2_flag  = 1, // which paramter we're floating
                               Double_t &min_param1, Double_t &min_param2,
                               const string metric_flag = "chi2", // which metric we're minimizing - change to just do both at once?
                               Double_t precision       = 0.01){

    if(metric_flag != "chi2" && metric_flag != "likeli"){
        cout << "Not a valid metric choice to minimize." << endl;
        exit(-1);
    }

    srand(time(0));
    delete gRandom;
    TRandom3 *randnum     = new TRandom3(0);
    Double_t *metric_vals = new Double_t[9];
    Double_t *descent     = new Double_t[9];
    Double_t rand_param1  = 0.;
    Double_t rand_param2  = 0.;

// Generate random point in our range to start at
    rand_param1 = ceil(randnum->Uniform(start1+step,stop1-step)*10.)/10.; //if require step to be a power of 10, can do *(1/step))/(1/step); just to avoid having to change it twice if you want to change the accuracy
    rand_param2 = ceil(randnum->Uniform(start2+step,stop2-step)*10.)/10.;

// Set function parameters and calculate chi squared or likelihood below, at, and above the random parameter choices (9 values) for the first time
    rand_param1 -= step;
    rand_param2 -= step;
    for(int i = 0; i < 3; ++i){
        for(int j = 0; j < 3; ++j){
            func -> SetParameter(param1_flag,rand_param1);
            func -> SetParameter(param2_flag,rand_param2);
            if(metric_flag == "chi2") metric_vals[(3*i) + j] = calc_chi2(data_hist, func, data_start, data_stop, bin_width, num_bins);
            else                      metric_vals[(3*i) + j] = calc_likelihood(data_hist, func, data_start, data_stop, bin_width, num_bins);
            rand_param2 += step;
        }
        rand_param1 += step;
        rand_param2 -= 3*step;
    }
    rand_param1 -= 3*step;

// Begin infinite loop finding min
    while(1){
// Set the descent values
        for(int i = 0; i < 9; ++i) descent[i] = metric_vals[4] - metric_vals[i];

// Find which direction is largest decreasing (or if we're at the minimum) and step accordingly
        Int_t step_direction = 0;
        Double_t descent_max = descent[0];
// 0: 1-,2-; 1: 1-,2; 2: 1-,2+
// 3: 1 ,2-;          5: 1 ,2+
// 6: 1+,2-; 7: 1+,2; 8: 1+,2+
        for(int i = 1; i < 9; ++i) if(i != 4 && descent[i] > descent_max){
            descent_max    = descent[i];
            step_direction = i;
        }

// Make check now to see if we're at the minimum
        if(descent_max <= 0){ // is it the minimum?
            min_param1 = rand_param1;
            min_param2 = rand_param2;
            break;
        }

// If not, continue looking for it
// Move parameters in direction of deepest descent
// *****THINK ABOUT THIS AND FIX: what if minimum is ON the boundary? I think currently this would create an infinite loop, add something to avoid this from happening
        if(step_direction == 0 || step_direction == 1 || step_direction == 2){
            if(rand_param1 > start1) rand_param1 -= step;
        }else if(step_direction == 6 || step_direction == 7 || step_direction == 8){
            if(rand_param1 < stop1) rand_param1 += step;
        }
        if(step_direction == 0 || step_direction == 3 || step_direction == 6){
            if(rand_param2 > start2) rand_param2 -= step;
        }else if(step_direction == 2 || step_direction == 5 || step_direction == 8){
            if(rand_param2 < stop2) rand_param2 += step;
        }

// Set next values
        if(step_direction == 0){
            metric_vals[4] = metric_vals[0];
            metric_vals[5] = metric_vals[1];
            metric_vals[7] = metric_vals[3];
            metric_vals[8] = metric_vals[4];
            func -> SetParameter(param1_flag,rand_param1-step);
            func -> SetParameter(param2_flag,rand_param2-step);
            if(metric_flag == "chi2") metric_vals[0] = calc_chi2(data_hist, func);
            else                      metric_vals[0] = calc_likelihood(data_hist, func);
            func -> SetParameter(param2_flag,rand_param2);
            if(metric_flag == "chi2") metric_vals[1] = calc_chi2(data_hist, func);
            else                      metric_vals[1] = calc_likelihood(data_hist, func);
            func -> SetParameter(param2_flag,rand_param2+step);
            if(metric_flag == "chi2") metric_vals[2] = calc_chi2(data_hist, func);
            else                      metric_vals[2] = calc_likelihood(data_hist, func);
            func -> SetParameter(param1_flag,rand_param1);
            func -> SetParameter(param2_flag,rand_param2-step);
            if(metric_flag == "chi2") metric_vals[3] = calc_chi2(data_hist, func);
            else                      metric_vals[3] = calc_likelihood(data_hist, func);
            func -> SetParameter(param1_flag,rand_param1+step);
            if(metric_flag == "chi2") metric_vals[6] = calc_chi2(data_hist, func);
            else                      metric_vals[6] = calc_likelihood(data_hist, func);
        }else if(step_direction == 1){
            metric_vals[3] = metric_vals[0];
            metric_vals[4] = metric_vals[1];
            metric_vals[5] = metric_vals[2];
            metric_vals[6] = metric_vals[3];
            metric_vals[7] = metric_vals[4];
            metric_vals[8] = metric_vals[5];
            func -> SetParameter(param1_flag,rand_param1-step);
            func -> SetParameter(param2_flag,rand_param2-step);
            if(metric_flag == "chi2") metric_vals[0] = calc_chi2(data_hist, func);
            else                      metric_vals[0] = calc_likelihood(data_hist, func);
            func -> SetParameter(param2_flag,rand_param2);
            if(metric_flag == "chi2") metric_vals[1] = calc_chi2(data_hist, func);
            else                      metric_vals[1] = calc_likelihood(data_hist, func);
            func -> SetParameter(param2_flag,rand_param2+step);
            if(metric_flag == "chi2") metric_vals[2] = calc_chi2(data_hist, func);
            else                      metric_vals[2] = calc_likelihood(data_hist, func);
        }else if(step_direction == 2){
            metric_vals[3] = metric_vals[1];
            metric_vals[4] = metric_vals[2];
            metric_vals[6] = metric_vals[4];
            metric_vals[7] = metric_vals[5];
            func -> SetParameter(param1_flag,rand_param1-step);
            func -> SetParameter(param2_flag,rand_param2-step);
            if(metric_flag == "chi2") metric_vals[0] = calc_chi2(data_hist, func);
            else                      metric_vals[0] = calc_likelihood(data_hist, func);
            func -> SetParameter(param2_flag,rand_param2);
            if(metric_flag == "chi2") metric_vals[1] = calc_chi2(data_hist, func);
            else                      metric_vals[1] = calc_likelihood(data_hist, func);
            func -> SetParameter(param2_flag,rand_param2+step);
            if(metric_flag == "chi2") metric_vals[2] = calc_chi2(data_hist, func);
            else                      metric_vals[2] = calc_likelihood(data_hist, func);
            func -> SetParameter(param1_flag,rand_param1);
            if(metric_flag == "chi2") metric_vals[5] = calc_chi2(data_hist, func);
            else                      metric_vals[5] = calc_likelihood(data_hist, func);
            func -> SetParameter(param1_flag,rand_param1+step);
            if(metric_flag == "chi2") metric_vals[8] = calc_chi2(data_hist, func);
            else                      metric_vals[8] = calc_likelihood(data_hist, func);
        }else if(step_direction == 3){
            metric_vals[1] = metric_vals[0];
            metric_vals[2] = metric_vals[1];
            metric_vals[4] = metric_vals[3];
            metric_vals[5] = metric_vals[4];
            metric_vals[7] = metric_vals[6];
            metric_vals[8] = metric_vals[7];
            func -> SetParameter(param1_flag,rand_param1-step);
            func -> SetParameter(param2_flag,rand_param2-step);
            if(metric_flag == "chi2") metric_vals[0] = calc_chi2(data_hist, func);
            else                      metric_vals[0] = calc_likelihood(data_hist, func);
            func -> SetParameter(param1_flag,rand_param1);
            if(metric_flag == "chi2") metric_vals[3] = calc_chi2(data_hist, func);
            else                      metric_vals[3] = calc_likelihood(data_hist, func);
            func -> SetParameter(param1_flag,rand_param1+step);
            if(metric_flag == "chi2") metric_vals[6] = calc_chi2(data_hist, func);
            else                      metric_vals[6] = calc_likelihood(data_hist, func);
        }else if(step_direction == 5){
            metric_vals[0] = metric_vals[1];
            metric_vals[1] = metric_vals[2];
            metric_vals[3] = metric_vals[4];
            metric_vals[4] = metric_vals[5];
            metric_vals[6] = metric_vals[7];
            metric_vals[7] = metric_vals[8];
            func -> SetParameter(param1_flag,rand_param1-step);
            func -> SetParameter(param2_flag,rand_param2+step);
            if(metric_flag == "chi2") metric_vals[2] = calc_chi2(data_hist, func);
            else                      metric_vals[2] = calc_likelihood(data_hist, func);
            func -> SetParameter(param1_flag,rand_param1);
            if(metric_flag == "chi2") metric_vals[5] = calc_chi2(data_hist, func);
            else                      metric_vals[5] = calc_likelihood(data_hist, func);
            func -> SetParameter(param1_flag,rand_param1+step);
            if(metric_flag == "chi2") metric_vals[8] = calc_chi2(data_hist, func);
            else                      metric_vals[8] = calc_likelihood(data_hist, func);
        }else if(step_direction == 6){
            metric_vals[1] = metric_vals[3];
            metric_vals[2] = metric_vals[4];
            metric_vals[4] = metric_vals[6];
            metric_vals[5] = metric_vals[7];
            func -> SetParameter(param1_flag,rand_param1-step);
            func -> SetParameter(param2_flag,rand_param2-step);
            if(metric_flag == "chi2") metric_vals[0] = calc_chi2(data_hist, func);
            else                      metric_vals[0] = calc_likelihood(data_hist, func);
            func -> SetParameter(param1_flag,rand_param1);
            if(metric_flag == "chi2") metric_vals[3] = calc_chi2(data_hist, func);
            else                      metric_vals[3] = calc_likelihood(data_hist, func);
            func -> SetParameter(param1_flag,rand_param1+step);
            if(metric_flag == "chi2") metric_vals[6] = calc_chi2(data_hist, func);
            else                      metric_vals[6] = calc_likelihood(data_hist, func);
            func -> SetParameter(param1_flag,rand_param1+step);
            func -> SetParameter(param2_flag,rand_param2);
            if(metric_flag == "chi2") metric_vals[7] = calc_chi2(data_hist, func);
            else                      metric_vals[7] = calc_likelihood(data_hist, func);
            func -> SetParameter(param2_flag,rand_param2+step);
            if(metric_flag == "chi2") metric_vals[8] = calc_chi2(data_hist, func);
            else                      metric_vals[8] = calc_likelihood(data_hist, func);
        }else if(step_direction == 7){
            metric_vals[0] = metric_vals[3];
            metric_vals[1] = metric_vals[4];
            metric_vals[2] = metric_vals[5];
            metric_vals[3] = metric_vals[6];
            metric_vals[4] = metric_vals[7];
            metric_vals[5] = metric_vals[8];
            func -> SetParameter(param1_flag,rand_param1+step);
            func -> SetParameter(param2_flag,rand_param2+step);
            if(metric_flag == "chi2") metric_vals[6] = calc_chi2(data_hist, func);
            else                      metric_vals[6] = calc_likelihood(data_hist, func);
            func -> SetParameter(param2_flag,rand_param2);
            if(metric_flag == "chi2") metric_vals[7] = calc_chi2(data_hist, func);
            else                      metric_vals[7] = calc_likelihood(data_hist, func);
            func -> SetParameter(param2_flag,rand_param2+step);
            if(metric_flag == "chi2") metric_vals[8] = calc_chi2(data_hist, func);
            else                      metric_vals[8] = calc_likelihood(data_hist, func);
        }else if(step_direction == 8){
            metric_vals[0] = metric_vals[4];
            metric_vals[1] = metric_vals[5];
            metric_vals[3] = metric_vals[7];
            metric_vals[4] = metric_vals[8];
            func -> SetParameter(param1_flag,rand_param1-step);
            func -> SetParameter(param2_flag,rand_param2+step);
            if(metric_flag == "chi2") metric_vals[2] = calc_chi2(data_hist, func);
            else                      metric_vals[2] = calc_likelihood(data_hist, func);
            func -> SetParameter(param1_flag,rand_param1);
            if(metric_flag == "chi2") metric_vals[5] = calc_chi2(data_hist, func);
            else                      metric_vals[5] = calc_likelihood(data_hist, func);
            func -> SetParameter(param1_flag,rand_param1+step);
            func -> SetParameter(param2_flag,rand_param2-step);
            if(metric_flag == "chi2") metric_vals[6] = calc_chi2(data_hist, func);
            else                      metric_vals[6] = calc_likelihood(data_hist, func);
            func -> SetParameter(param2_flag,rand_param2);
            if(metric_flag == "chi2") metric_vals[7] = calc_chi2(data_hist, func);
            else                      metric_vals[7] = calc_likelihood(data_hist, func);
            func -> SetParameter(param2_flag,rand_param2-step);
            if(metric_flag == "chi2") metric_vals[8] = calc_chi2(data_hist, func);
            else                      metric_vals[8] = calc_likelihood(data_hist, func);
        }
    }

    cout << metric_flag << endl;
    cout << "param1: " << min_param1 << endl;
    cout << "param2: " << min_param2 << endl;

    delete[] metric_vals;
    delete randnum;
}
