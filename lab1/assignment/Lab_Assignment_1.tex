\documentclass{article}
\usepackage{color}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{epstopdf}
\usepackage[figuresright]{rotating}

\begin{document}

	\begin{center}
		PHYS 689.601: Methods of Experimental Particle Physics. Fall 2015. \\ Lab Assignment 1\\
	\end{center}

	\begin{center}

		{\bf Numeric Integration}\\
	\end{center}

	
	\vskip6mm
	
	\begin{center}
		Instructions
	\end{center}
	\begin{itemize}
		\item Please submit your homework by:
		\begin{itemize}
			\item copying it to \textcolor{blue}{"submit"} directory
			\item changing access rights to it: \textcolor{blue}{"chmod 740 submit/$<$yourfile$>$"}
		\end{itemize}
	\end{itemize}
	
	\vskip6mm	
	
	\begin{center}
		Links and References:
	\end{center}
	
	\begin{itemize}
		\item User's Guide: \href{http://root.cern.ch/drupal/content/users-guide}{http://root.cern.ch/drupal/content/users-guide}
		\begin{itemize}
			\item Trees: \href{http://root.cern.ch/download/doc/12Trees.pdf}{http://root.cern.ch/download/doc/12Trees.pdf}
		\end{itemize}
		\item Reference Guide: \href{http://root.cern.ch/root/html534/ClassIndex.html}{http://root.cern.ch/root/html534/ClassIndex.html}
		\begin{itemize}
			\item TTree: \href{http://root.cern.ch/root/html534/TTree}{http://root.cern.ch/root/html534/TTree}
			\item TRandom: \href{http://root.cern.ch/root/html534/TRandom}{http://root.cern.ch/root/html534/TRandom}
			\item TCut: \href{http://root.cern.ch/root/html534/TCut}{http://root.cern.ch/root/html534/TCut}
		\end{itemize}
	\end{itemize}
	
	\vskip6mm	
	
	\begin{center}
		Examples
	\end{center}		
		
	\vskip6mm	

\section{Numeric Calculations} 

Numeric calculations are widely used in experimental particle physics. While many such calculations rely nowadays on standard libraries and routines written by someone long time ago, it is important to have a good feeling of how these methods work and recognize cases when the standard routines fail. One of the most typical problems one frequently has to tackle in experimental HEP is numeric integration, which this assignment is dedicated to.

\section{Classical Integration Algorithms}

Numeric integration is a problem that has been around for a long time and have been studied extensively over the years. While the modern algorithms are usually highly optimized for fast (and correct) evaluation of integrals, simple "legacy" algorithms still work pretty well in most cases and provide a good illustration of typical problems one has to be mindful of. 

The most typical examples of such legacy algorithms iare the {\it midpoint rule} and the {\it trapesoidal rule}. The first one evaluates the integral by dividing the range of integration into a number of bins $N_{bin}$ and for each bin $i$ it evaluates the value of the function $f_i$ in the center of the bin $x_i$. If the bin sizes 
$\delta x_i$ are known, the estimator for the value of the integral is a sum:
\begin{eqnarray}
\sum_{i=1}^{N_{bin}}{f_i \delta x_i}
\end{eqnarray}

The {\it trapesoidal rule} is a slightly optimized version of the first algorithm, which takes into account the slopes of the function being integrated, which speeds up the convergence for cases with fast changing functions. Instead of just taking the value of the function at the middle of a bin, it takes the end-points of the bin and calculates the area under a straight line connecting the endpoints. Summing up contributions from each bin, one arrives to:
\begin{eqnarray}
\sum_{i=1}^{N_{bin}}{(f(x_i - \delta x_i/2)+f(x_i+\delta x_i/2))/2 \times  \delta x_i}
\end{eqnarray}

In both cases, one starts with some initial value for the number of bins, calculates the integral, makes the number of bins larger and re-calculates the integral. As the algorithm iterates, it watches how much the calculated value of the integral changes from one iteration to another. Once you see that the variations become smaller than a certain pre-determined value (which is the desired accuracy of your calculation), you stop. There are dangers involved with such simple approach as if the function you are integrating has a very narrow peak, if the number of iterations is too small, if none of the points at which you probed the function was in the vicinity of the peak, the algorithm can stop too early yielding a vastly incorrect result. However, if it is known that your function does not have singularities, it will work pretty well. If you know where the singularities are located, you may want to divide the region into smaller regions and use larger bin sizes for the ``easy'' regions and smaller bin sizes for the ``difficult regions''.

\section{Monte Carlo Integration}
Integration is one of the simplest examples where the Monte Carlo (MC) technique can be very useful. One can show that the MC converges faster than the ``old school'' techniques like trapezoidal integration etc. Many of the calculations related to quantifying model predictions in experimental high energy physics are in fact integration problems, even though one may not always realize that. For example, a typical task of simulating a sample describing a particular (signal or background) process including emulation of the detector performance is essentially an attempt to take an integral that in a slightly simplified way could look like the following:
\begin{eqnarray}
N(x_{k+1}, ..., x_{N})= \int{\frac{d^N \sigma(\vec{x})}{dx_1 ... dx_N}  R(\vec{x}, \vec{y}) {dx_i ... dx_k} {dy_1 ...dy_m}}
\end{eqnarray}
where vector $x$ encompasses all the information needed to describe the physical process, including the number of particles, their momentum and directions, $\sigma$ is the effective cross-section (can be a convolution of parton level cross-section with parton distribution functions, beam energy probability density functions etc.); vector $y$ includes all parameters needed to describe the response of the detector, $R(x,y)$ is the response function of the detector including all uncertainties in the knowledge of the response. Note the non-integrated parameters $x$ in $\sigma(x)$ and $R(x,y)$: these would correspond to tthe parameters that you are planning to look at in your analysis while the rest is integrated out. For a realistic example, taking an integral of this sort would be an insanely difficult task. Thanks to the MC techniques, one can avoid this problem by generating a sample of ``simulated events'' to predict the properties of the actual data events produced via this process and as observed in the detector. We do this all the time when we run a particle event generator followed by a GEANT-based detector emulation program.

\subsection{Monte Carlo Integration}

The basic point that the MC relies on is that an integral of a function over a range is equal to $<f(x)>_{ab}$, the average value of that function over that range, times the size of the range:
\begin{eqnarray}
\int_{a}^{b}{f(x) dx} = <f(x)>_{ab} \times (b-a)
\end{eqnarray}
This converts the problem of calculating an integral into a problem of finding the average value of a given function $f$ on the interval in question and with a given accuracy. To calculate the average value numerically, you generate random (and uniformly distributed) numbers $x_i$ in the range $a<x<b$, every time calculating $f(x_i)$ and then computing the average $<f(x)>_{ab}$ over all the iterations you made. It is trivial to extend this to multi-dimensional integration, in that case you simply multiply the average function value by the size of the volume you are integrating over.

As you go through the iterations, you would want to know when you can stop as you have reached the desired accuracy in your numeric calculation. It is easy to develop a simple math to evaluate the accuracy of the integral:
\begin{eqnarray}
I/(b-a) = <f(x)> = \bar{f}=\frac{1}{N} \sum_{i=1}^{N}{f(x_i)},
\end{eqnarray}
where we switched to the notation $\bar{f}$ for the average. You can think of this calculation as making many measurements $f_i$ and finding the mean of the distribution. Note that $\delta f$, the variance of $f$, defined as 
\begin{eqnarray}
<(\delta f)^2 >= <{(f-\bar{f})^2>= \frac{1}{N} \sum_{n=1}^{N}{({f}_n - \bar{f})^2} = \frac{1}{N} \sum_{n=1}^{N}{(({f}_n)^2 - 2 {f}_n \bar{f}+\bar{f}^2)}}\\
=  \frac{1}{N} \sum_{n=1}^{N}{(({f}_n)^2 - \bar{f}^2)}= \bar{f^2} - {\bar{f}}^2
\end{eqnarray}
is not decreasing with the number of measurements $N$. The variance is the analogue of the width of a distribution in the analogy where you are estimating some value $f$ by taking many measurements, in which case the width is representative of the accuracy of your measurements. In our case, this ``width'' is the reflection of how ``difficult'' the function you are integrating is. It is intuitively clear if you imagine plotting your measurements of $f_i$ on a histogram (the $x$ axis will be the values $f_i$), you will get a distribution and as you add more and more measurements, you will be better seeing the shape, but the distribution itself will not be becoming any narrower.

To figure out how to calculate the uncertainty of the mean, you could think of doing it statistically by staging $K$ measurements of $\bar{f}$, which we will denote as $F_k$, and then estimate the new ``global mean'' $\bar{F}$, which should be an improved measurement of the integral, as:
\begin{eqnarray}
\bar{F} =  \frac{1}{K} \sum_{k=1}^{M}{{F}_k}
\end{eqnarray}
The uncertainty in $F$ can be estimated in the usual fashion:
\begin{eqnarray}
\delta F^2 = <{(F-\bar{F}})^2>= \bar{F^2} - \bar{F}^2
\end{eqnarray}

By substituting the explicit expressions for $F_k$ in terms of a sums of $f_n$, you can find out that
\begin{eqnarray}
<\delta F^2 >=\frac{1}{KM}<(\delta f)^2 >=\frac{1}{N}<(\delta f)^2 >,
\end{eqnarray}
where $N=KM$ is the total number of measurements of $f$ done so far.

This last expression gives a simple way to control the accuracy of your calculation ``on the fly'': you should simply keep track of $f^2$ and $f$ sums. From time to time, you will pause the algorithm to evaluate the relative uncertainty in the measurement of the mean:
\begin{eqnarray}
\Delta I/I= \sqrt{<\delta F^2 >}/\bar{F}
\end{eqnarray}
and continue until your estimate of the uncertainty reaches the required accuracy (say 0.01\%), then you break out of the loop and report the result.

\subsection{Integrating ``Difficult'' Functions}
The most unpleasant cases are when the function changes a lot over the range of integration or has (pseudo-)singularities within the range of your calculations. In this case, $<(\delta f)^2 >$ will be large and the convergence of your procedure will be slow. One trick to increase the speed of convergence is to change how you generate $x_i$: if instead of generating it uniformly, you more frequently probe the regions where the function changes its value fast reaching large values (and thus these regions dominate the calculated integral value), and less frequently those where the function is small. In an ideal (but also trivial) case, if you knew the distribution you are integrating analytically, you could generate $x_i$ according to that exact distribution. However, if you choose to generate events non-uniformly, you should apply a weight to each event, as otherwise your final sum will be a correct answer for another integral. Generally, if $x_i$ is randomly generated according to function $w(x)$, each entry $i$ in your sum should be weighed by $1/w(x_i)$ to compensate for the non-uniformity of the distribution. The calculation itself will not change much except that in the previous formulas you will replace $f_i \to f_i/w_i$. Because the function has effectively changed, the new ``function'' can have smaller variance and thus converge faster.

In extreme cases, you may ``physically'' divide the integration region into sub-regions, e.g. make a region around each pseudo-singularity and the rest combine into a  single``bulk'' region, then calculate integrals for each region separately and sum them up. This way you will spend most of your CPU in the regions that are difficult until you get desired precision and evaluate the integral n the bulk region with a small number of ``measurements''. You can also consider generating your measurements following a distribution that more or less resembles the shape of the function in the difficult region. Modern integration algorithms, including some available in ROOT, often ``test'' the function to identify difficult regions and/or ``scan'' the shape of the function and then generate ``measurements'' following a distribution that is similar to the function being integrated.

ROOT has various random number generators that can generate numbers according to either a pre-defined function or even an arbitrary function you pass to ROOT as a formula or even a histogram. The algorithms generating numbers according to a pre-defined function are usually faster, which can make a difference in complex calculations.

\subsection{Multi-Dimensional Integrals}
It is trivial to extend the above onto a multi-dimensional case. In a 2-dimensional case you will still be probing your function $f$, but in a 2-dimensional space by generating a pair of random and uniform numbers $x_i$ and $y_i$ and calculating the ``average'' value of your function over the 2-dimensional region of integration.

\subsection{Practical Notes}
In some cases, your summation can include very large and very small numbers. If you think how the computer adds such numbers, given that each number can only take a certain number of bits, you will realize that in the extreme case you can get $A+a=A$. That can be okay in some cases, but imagine your integral calculation actually looks more like $A+N\times a$ where $N$ is a very large number, so that the combined ``$a$-contribution'' can even be much larger than $A$. However, because your calculation is done as $((A+a)+a)+a ...$, the result will remain as $A$ leading you to a mistake in your calculations. A simple but practical way out is to sum small numbers separately from large numbers and then, at the end, sum up the results of each summation into the final sum.

Another example is the calculation of likelihood functions, which are essentially a product of a large number of values that can range from small to incredibly small. Mistakes in such multiplications can be very large if not done carefully. A practical solution would be to calculate logarithms of each number and them sum them up. Logarithms will make the numbers closer to each other in magnitude making the calculation more stable.

If you need to calculate the value of an integral if the region of integration is not closed, e.g. from $0$ to $\infty$, you obviously can't generate an infinite number of bins or generate random numbers from zero to infinity. One solution to such problem would be to first change variables, e.g. using $y=x/(x+1)$ will change the limits of integration for y from 0 to 1. If you were doing that change of variables analytically, you would need to include the Jacobian of such transformation under the integral. In numeric calculations, you would use a weight to effectively modify the function you are integrating.

\section{Lab Assignment}

\subsection{Part 1: Warm-up}
Write code that calculates the integral of a function with a given precision using each of the three types of integration discussed above. To keep your code well organized, you will likely want to divide your script into modules (methods) which return the value of the function, so that you can later easily replace your function with another function. Start with simple functions with the region of integration from 0 to 1: 
\begin{itemize}
\item $f(x)= 1$;
\item $f(x) = x$;
\item $f(x)= x^3$
\end{itemize}
and make sure that results make sense (as you know what the answer should be). Make a comparison of the convergence rate of the integral as a function of the number of iterations. To make the definition of iterations comparable for all three methods, it is convenient to define it as the number of times you probe the value of the function. You will want to make a plot of the current value of the integral at certain values of the number of iterations, and plot the results in the estimator $I(N)$ versus N plot. Evaluate the rate of the convergence in each case and discuss your findings.

For MC integration, you should also generate the following plots (histograms) on a single canvas:
\begin{itemize}
\item Current value of the integral $I=<f(x)>_{(0:1)}$ as a function of the number of iterations $N$ (you will want to make entries in the histogram for some discrete values of $N$, like every 100 or 1000 iterations;
\item Current rms of $I$ (see the above discussion) as a function of $N$;
\item Current uncertainty of the calculated $I$ as a function of $N$;
\end{itemize}

Discuss and explain your findings.
 
{\it Start working on your lab report, which should be completed in LaTex and for this part should include Figures (see the appendix of one of the homework assignments for an example of using figures, note the file format that you will want ROOT to produce your figures in) where you will show the above canvas and include a brief discussion of whether the results make sense to you or not and why.}

\subsection{Part 2: A realistic integration example}
Next week you will be given two ``more difficult'' functions. You will need to calculate its integral using the MC method above using flat sampling of the region of integration and repeat the same type of ``monitoring'' measurements you have done in the first part of the exercise. In one case, the integration will need to be performed over a closed region of space, in the other case the range will be half-open.

Next, for each of the two cases you will need to think of at least two simple enough functional forms for sampling in order to increase the speed of the convergence. You will need to perform a comparative analysis of the convergence speeds for the flat and the two ``optimized'' choices, explain your choices and make conclusions. In addition to measuring the speed of the algorithms using number of iterations, you will also need to measure it in terms of the CPU time. Check if the conclusions you draw using these two figures of merit  (CPU time versus $N_{iter}$ are the same. Discuss if they always have to be the same or there could be cases when the two figures of merit contradict on the question of which algorithm is better. 

\section{Presentation}

Your findings should be written in a form of a mini-paper, which should include an introduction explaining what you are studying, sections describing your measurements and showing the results (plots, tables), as well as the conclusions. Do not forget to include references if you use external literature or tools (The ROOT package itself should deserve a reference, right?). 

Below is an example of placing a figure in a document.


\begin{figure}[htb]
\includegraphics[width=0.95\linewidth]{fig.pdf}
\caption{Feynman diagram for the QED process $e^- \mu^- \to e^- \mu^-$.}
\label{RespFunc}
\end{figure}


\end{document}