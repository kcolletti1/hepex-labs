1DIMENISONAL FITS

p0^2:
  EXT PARAMETER                                   STEP         FIRST   
  NO.   NAME      VALUE            ERROR          SIZE      DERIVATIVE 
   1  p0           1.01321e+00   2.70726e-01   1.85632e-04   1.59659e-03

p1:
  NO.   NAME      VALUE            ERROR          SIZE      DERIVATIVE 
   1  p0           2.15363e+01   1.51168e+00   2.72578e-04  -1.27230e-04

p2:
  NO.   NAME      VALUE            ERROR          SIZE      DERIVATIVE 
   1  p0           2.12426e+01   2.47830e-01   5.89670e-05   3.60083e-02

p3:
  NO.   NAME      VALUE            ERROR          SIZE      DERIVATIVE 
   1  p0           3.94651e+00   1.74819e-01   7.10523e-05   1.11476e-02


2DIMENSIONAL FITS

p0^2 and p1:
  NO.   NAME      VALUE            ERROR          SIZE      DERIVATIVE 
   1  p0           1.00399e+00   2.99768e-01   1.41657e-04   2.18409e-02
   2  p1           2.51213e+01   1.67349e+00   2.26139e-04  -1.18243e-02

p1 and p2:


p1 and p3:


p2 and p3:

