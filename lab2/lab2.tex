\documentclass{article}

\usepackage[margin=0.75in]{geometry}
\usepackage{amsmath}
\usepackage{color}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{subfigure}
\usepackage[figuresright]{rotating}
\usepackage{mathtools}

\begin{document}

\begin{center}
PHYS 689.601: Methods of Experimental Particle Physics. Fall 2015. \\ Lab Assignment 2\\
\end{center}

\begin{center}
{\bf Fitting 1-D and 2-D Functions to Data - Comparing Performance of \(\chi^2\) and Likelihood Fits}\\
\end{center}

\begin{center}
{\bf Katrina Colletti}\\
\end{center}
	
\begin{center}
{\bf \today}\\
\end{center}
	
\vskip6mm

\section{Introduction}
In this lab, we are given a sample data file and a test function with four parameters,
\[f(x;p_0^2,p_1,p_2,p_3)=p_0^2+p_1\exp\big(-(x-p_2)^2/(2p_3^2)\big),\]
to fit to the data. We use both the \(\chi^2\) minimization method and the maximal likelihood method to fit the given function to the data. We first fix three of the parameters and leave one floating and find the best fit to the data, and then we fix two of the parameters and leave the other two floating and perform a 2-dimensional best fit to the data. The general idea is to choose a function that generally fits the shape of the data you're trying to fit to but leave some number of parameters of the function unfixed. You then loop over a range of values that the unspecified parameter could take, and compare the value of the function with the value of the data at each bin that you have. The actual comparison of the function values and the data values depend on the metric you are using to quantify the fit. Below I will briefly discuss the two metrics that we explore in this lab.
 
\section{Methods}
\subsection{Minimizing \(\chi^2\) - General Overview}
The \(\chi^2\) function is given by
\[\chi^2=\sum_{i=1}^{N_\text{bins}}\frac{\big(f_i-D_i\big)^2}{2\sigma^2}.\]
\(N_{\text{bins}}\) is the number of bins that are in your data sample. The \(D_i\) values are the values of your data in each bin, and the \(f_i\) values are the values of the function sampled at the middle of the bin. \(\sigma\) is the width that you would expect from a measurement, since there is natural smearing when you measure any physical quantity experimentally, and so in this case it is given by \(\sigma=\sqrt{f_i}\). This is valid when \(f_i\) is sufficiently large, but when the function value gets small enough, this will not be completely accurate. We will see in the results section where this inaccuracy actually affects results. The \(\chi^2\) metric tells us how good of a fit the function is to the data, and so when you calculate \(\chi^2\) for a varying value of a parameter, the minimal value of \(\chi^2\) will correspond to the parameter that gets the function to fit the data the best out of the range that was tested, and of course only out to the precision given by the step size that you test. One last thing to point out that is important for the studies done in this lab is that when you find the minimal value for the parameter that fits best to the data, the uncertainty on that parameter can be obtained from the range of parameter values that correspond to the \(\chi^2\) value only changing by \(1/2\). For example, if you found that \(p=4\) gives you the minimal \(\chi^2\) value of 5, and at \(p=3.2\) and \(p=4.9\) the \(\chi^2\) value was 5.5, then the uncertainty on that parameter would be given by \(\,^{+0.9}_{-0.8}\).

\subsection{Maximizing Likelihood - General Overview}
The likelihood metric is given by
\[L=\Pi_{i=1}^{N_{\text{bins}}}P(f_i,D_i),\]
which we can rewrite as
\[-\log(L)=-\sum_{i=1}^{N_{\text{bins}}}\log(P(f_i,D_i)),\]
with \(P(\nu,N)\) being the Poisson distribution, \(P(\nu,N)=\frac{\nu^Ne^-\nu}{N!},\) and again \(f_i\), \(N_{\text{bins}}\), and \(D_i\) the same as previously defined. This is a similar metric to the \(\chi^2\) metric, but as the name suggests, you want to maximize the likelihood that the parameter fits the data well. Another key difference is also that it is still accurate when the value of \(f_i\) is sufficiently small. In this lab for the 2-Dimensional plots, we will take the negative of the log of \(L\) (just to make the plotting easier/prettier), and therefore also note that we will again want to minimize the value we obtain from our tests. First, the reason we take the log of the likelihood is that the numbers we obtain from this method are typically pretty small, and computationally it is easier dealing with "regular sized" numbers rather than tiny numbers, plus it makes the algorithm more stable. Taking the negative of this value is not necessary, however I do this because I would like to plot the z axis in my 2-D plots (representing the likelihood value) on a log scale to see the structure clearer, and I need to have the likelihood value be positive in order to do this so I simply multiply by -1. The uncertainty on the parameter value we find is given in the same way as the way we find it for the \(\chi^2\) method.

\section{Discussion of Results and Conclusions}
\subsection{General Discussion}
In Fig.~\ref{fig:data}, we see the data that we use in this lab to perform fits to.
\begin{figure}[htb]
\centering{
\includegraphics[width=0.4\textwidth,keepaspectratio]{figures/data.pdf}
}
\caption{Sample data given in this lab to optimize fits through coding and provided functionality methods in ROOT via the fit panel.}
\label{fig:data}
\end{figure}

The function that we use to fit to this data is, as mentioned earlier, given by
\[f(x;p_0^2,p_1,p_2,p_3)=p_0^2+p_1\exp\big(-(x-p_2)^2/(2p_3^2)\big).\]

For the 1-Dimensional fits, we use the following table (Table~\ref{table:1d}).
\begin{table}[h]
\centering{
\begin{tabular}{|c|c|c|c|c|}\hline
 & \(p_0^2\) & \(p_1\) & \(p_2\) & \(p_3\)\\\hline
1 & -- & 25.0 & 20.4 & 4.8\\\hline
2 & 2.5 & -- & 20.4 & 4.8\\\hline
3 & 2.5 & 25.0 & -- & 4.8\\\hline
4 & 2.5 & 25.0 & 20.4 & --\\\hline
\end{tabular}
}
\caption[]{Combination of parameters used to perform fits to the data. These are 1-Dimensional fits, meaning all parameters except for one are fixed in our function, and the remaining one is left to vary over a certain range.}
\label{table:1d}
\end{table}

Similarly, for the 2-Dimensional fits, we use the following table (Table~\ref{table:2d}).
\begin{table}[h]
\centering{
\begin{tabular}{|c|c|c|c|c|}\hline
 & \(p_0^2\) & \(p_1\) & \(p_2\) & \(p_3\)\\\hline
1 & -- & -- & 20.4 & 4.8\\\hline
2 & 2.5 & -- & -- & 4.8\\\hline
3 & 2.5 & -- & 20.4 & --\\\hline
4 & 2.5 & 25.0 & -- & --\\\hline
\end{tabular}
}
\caption[]{Combination of parameters used to perform fits to the data. These are 2-Dimensional fits, meaning all parameters except for two are fixed in our function, and the remaining two are left to vary over certain ranges.}
\label{table:2d}
\end{table}

First, we fit these various combinations of our function to the data with both the \(\chi^2\) and likelihood methods by simply calculating all of the values in the specified parameter ranges and keeping track of the minimum value in the code. Then, we use a more efficient minimization algorithm to find where the minimum values of the \(\chi^2\) and the likelihood functions are. I will describe the algorithm I use later, but this is more efficient because you don't need to calculate every value of \(\chi^2\) and likelihood in the specified range since it employs smarter methods to find where the minimum is -- though it does depend on the fact that what is being minimized decreases monotonically around it's minimum.
%\clearpage

\subsection{1-D and 2-D Fits - Simple Method}
See Figs.~\ref{fig:p0_1d}, \ref{fig:p1_1d}, \ref{fig:p2_1d}, and \ref{fig:p3_1d} for the histograms that I was able to produce for this part of the lab. Each portrays both the \(\chi^2\) and the likelihood behavior as one of the parameters of our function is varied over a specified range with a finite step size. For the 1-Dimensional cases, I use a step size of 0.01 over all of the ranges that I test. As an example of how you would visually estimate the minimum values and their associated uncertainty by eye, please see Fig.~\ref{fig:p0_1d_visual_ex}. To be able to do this programmatically, simple additions in my code of variables to keep track of the index of the arrays (which keep track of the parameter values, the \(\chi^2\) values, and the \(-\log(L)\) values) at which the minimum \(\chi^2\) and \(-\log(L)\) occurred allowed me to calculate these results very easily. Once the main loop was finished evaluating all of the parameter combinations, knowing the index in the arrays allowed me to iterate both up and down in each array until the specified deviation from the minimum occurred, which in turn told me what the uncertainty on the parameter values were. Results from this portion of the lab follow in Table~\ref{table:resultspart1}.

\begin{figure}[htb]
\centering{
\includegraphics[width=0.95\textwidth,keepaspectratio]{figures/p0_1d.pdf}
}
\caption{Parameter \(p_{0}^{2}\) allowed to vary over the range shown in the plot. The minimal value of the curve corresponds to the value of the parameter that makes the given function fit the data the best, as quantified by each metric.}
\label{fig:p0_1d}
\end{figure}

\begin{figure}[htb]
\centering{
\includegraphics[width=0.95\textwidth,keepaspectratio]{figures/p1_1d.pdf}
}
\caption{Parameter \(p_{1}\) allowed to vary over the range shown in the plot. The minimal value of the curve corresponds to the value of the parameter that makes the given function fit the data the best, as quantified by each metric.}
\label{fig:p1_1d}
\end{figure}

\begin{figure}[htb]
\centering{
\includegraphics[width=0.95\textwidth,keepaspectratio]{figures/p2_1d.pdf}
}
\caption{Parameter \(p_{2}\) allowed to vary over the range shown in the plot. The minimal value of the curve corresponds to the value of the parameter that makes the given function fit the data the best, as quantified by each metric. We see some interesting behavior that we don't see with the other parameters here, where the function is not monotonically increasing over the broad parameter range shown.}
\label{fig:p2_1d}
\end{figure}

\begin{figure}[htb]
\centering{
\includegraphics[width=0.95\textwidth,keepaspectratio]{figures/p3_1d.pdf}
}
\caption{Parameter \(p_{3}\) allowed to vary over the range shown in the plot. The minimal value of the curve corresponds to the value of the parameter that makes the given function fit the data the best, as quantified by each metric.}
\label{fig:p3_1d}
\end{figure}

\begin{figure}[htb]
\centering{
\includegraphics[width=0.95\textwidth,keepaspectratio]{figures/p0_1d_visual_example.pdf}
}
\caption{The plots for \(\chi^2\) and log likelihood for \(p_0^2\) floating, zoomed in around their minima showing the 1 sigma deviation in both more clearly.}
\label{fig:p0_1d_visual_ex}
\end{figure}

See Figs.~\ref{fig:p0p1_2d}, \ref{fig:p1p2_2d}, \ref{fig:p1p3_2d}, and \ref{fig:p2p3_2d} for the 2-D histograms that I was able to produce for this part of the lab. Each portrays both the \(\chi^2\) and the likelihood behavior as two of the parameters of our function are varied over specified ranges with a finite step size. For these 2-Dimensional cases, I use a step size of 0.1 over all of the ranges that I test. This is a larger step size than in the 1-Dimensional cases, but is still plenty enough precise for this lab to arrive at the correct results. I was able to get the results for the minima and uncertainty programmatically in precisely the same way as in the 1-D case, except I just needed an additional index tracker for the additional dimension. I consider each parameter independent and thus that is why it is simple to calculate these results in the code. See Fig.~\ref{fig:p0p1_2d_visual_ex} for a zoomed in version example of the plots where you can see the minimum clearly in addition to the one sigma deviation. The minimization results from this portion of the lab follow in Table~\ref{table:resultspart1}.

\begin{table}[h]
\centering{
\begin{tabular}{|c|c|c|c|c|}\hline
\(\mathbf{\chi^2}\) & \(p_0^2\) & \(p_1\) & \(p_2\) & \(p_3\)\\\hline
1 & 2.84(+0.41,-0.38) & fixed & fixed & fixed\\\hline
2 & fixed & 27.09(+1.69,-1.6) & fixed & fixed\\\hline
3 & fixed & fixed & 21.7(+0.32,-0.31) & fixed\\\hline
4 & fixed & fixed & fixed & 5.21(+0.24,-0.22)\\\hline
5 &  2.6(+0.5,-0.4) & 26.9(+1.7,-1.7) & fixed & fixed\\\hline
6 & fixed & 26.8(+1.7,-1.7) & 21.7(+0.4,-0.4) & fixed\\\hline
7 & fixed & 25.3(+1.6,-1.5) & fixed & 5.2(+0.3,-0.3)\\\hline
8 & fixed & fixed & 21.7(+0.4,-0.4) & 5.0(+0.3,-0.3)\\\hline
\end{tabular}
}
\centering{
\begin{tabular}{|c|c|c|c|c|}\hline
\(\mathbf{-\log(L)}\) & \(p_0^2\) & \(p_1\) & \(p_2\) & \(p_3\)\\\hline
1 & 1.1(+0.31,-0.29) & fixed & fixed & fixed\\\hline
2 & fixed & 22.04(+1.53,-1.5) & fixed & fixed\\\hline
3 & fixed & fixed & 22.07(+0.31,-0.31) & fixed\\\hline
4 & fixed & fixed & fixed & 4.07(+0.2,-0.2)\\\hline
5 & 1.1(+0.4,-0.3) & 24.7(+1.6,-1.6) & fixed & fixed\\\hline
6 & fixed & 23.5(+1.6,-1.6) & 22.1(+0.3,-0.4) & fixed\\\hline
7 & fixed & 26(+1.8,-1.8) & fixed & 4.0(+0.2,-0.2)\\\hline
8 & fixed & fixed & 22.0(+0.4,-0.4) & 4.2(+0.3,-0.2)\\\hline
\end{tabular}
}
\caption[]{Results for the minimal values of \(\chi^2\) and \(-\log(L)\) and their respective uncertainties given by where the metric value changes from it's minimum by 1/2. These results were calculated using the simple approach of calculating the metric values over the entire range and simply keeping track of where the minimum for each is.}
\label{table:resultspart1}
\end{table}

\begin{figure}[htb]
\centering{
\includegraphics[width=0.95\textwidth,keepaspectratio]{figures/p0p1_2d.pdf}
}
\caption{Parameters \(p_{0}^{2}\) and \(p_{1}\) allowed to vary over the ranges shown in the plot. The minimal value of the curve (which are now represented by contours) corresponds to the value of the parameter that makes the given function fit the data the best, as quantified by each metric.}
\label{fig:p0p1_2d}
\end{figure}

\begin{figure}[htb]
\centering{
\includegraphics[width=0.95\textwidth,keepaspectratio]{figures/p1p2_2d.pdf}
}
\caption{Parameters \(p_{1}\) and \(p_{2}\) allowed to vary over the ranges shown in the plot. The minimal value of the curve (which are now represented by contours) corresponds to the value of the parameter that makes the given function fit the data the best, as quantified by each metric.}
\label{fig:p1p2_2d}
\end{figure}

\begin{figure}[htb]
\centering{
\includegraphics[width=0.95\textwidth,keepaspectratio]{figures/p1p3_2d.pdf}
}
\caption{Parameters \(p_{1}\) and \(p_{3}\) allowed to vary over the ranges shown in the plot. The minimal value of the curve (which are now represented by contours) corresponds to the value of the parameter that makes the given function fit the data the best, as quantified by each metric.}
\label{fig:p1p3_2d}
\end{figure}

\begin{figure}[htb]
\centering{
\includegraphics[width=0.95\textwidth,keepaspectratio]{figures/p2p3_2d.pdf}
}
\caption{Parameters \(p_{2}\) and \(p_{3}\) allowed to vary over the ranges shown in the plot. The minimal value of the curve (which are now represented by contours) corresponds to the value of the parameter that makes the given function fit the data the best, as quantified by each metric.}
\label{fig:p2p3_2d}
\end{figure}

\begin{figure}[htb]
\centering{
\includegraphics[width=0.95\textwidth,keepaspectratio]{figures/p1p0_2d_visual_example.pdf}
}
\caption{The plots for \(\chi^2\) and log likelihood for \(p_0^2\) and \(p_1\) floating, zoomed in around their minima showing the 1 sigma deviation in both more clearly.}
\label{fig:p0p1_2d_visual_ex}
\end{figure}

\clearpage

\subsection{1-D and 2-D Functions - Finding the best fit more efficiently}
For this part of the lab, I employ an algorithm to find the minimum of a function within a specified range, but it does so in a more efficient way than going through each value within the range and keeping track of the minimum. A crucial aspect of this efficiency is the underlying assumption that not only is there one true minimum in the range, but also that the minimum point is the only critical point of the function within the range we're looking. The algorithms I implemented are a bit different for the 1-D and 2-D cases, so I'll explain them separately.

The algorithm I use to find the minimum for the 1-D case is relatively straight forward. I pick a random number within the parameter range, calculate the value of the metric that we're trying to minimize at that point and at the point below and above it. Depending on the direction that the function is decreasing, we then change the parameter range to cut off any points in the direction that the function was increasing, since we know that the minimum won't be there. We keep repeating this procedure until eventually the actual minimum is randomly chosen (note you could get super lucky and pick the minimum on the first few tries and the algorithm would be super computationally inexpensive, but of course the efficiency of algorithms should be based off of worst case scenario behavior) or we limit the range to be straddling the minimum point. This algorithm converges relatively quickly. All of my code in this lab ran pretty quickly, so without having larger ranges or functions that are harder to calculate to make the CPU time increase, it is hard to tell exactly how much quicker this method is relatively. It is almost certain though that the number of times the \(\chi^2\) or likelihood calculation is called will be significantly fewer than in the naive approach to just calculate all values in the range and find the smallest, since it's random and will knock out a lot of values that it doesn't need to ever calculate the metric for.

For the 2-D case, the algorithm I wrote to find the minimum of the function is a bit different. It picks a random point within the 2-Dimensional range from which to start at. It calculates the metric that we're trying to minimize at that point and all around this point (so there are 9 values it calculates in total). It finds which value yields the steepest descent and takes a step in that direction. It keeps doing this (and each time we don't need to recalculate all 9 metric values since we've already either calculated 4 or 6 of them in the pervious step so we just use those and only calculate the new ones to make it more efficient), and will eventually land on the minimum point. It knows when it lands on the minimum point when all of the "descent" calculated values are negative and this is when the loop in the algorithm stops. All of these algorithms that I wrote in this lab use a fixed precision that is set at the beginning and don't change any of the step sizes within the loops. Finally, it is important to make sure that in your parameter space that you're ranging over with this code that the function is monotonic around it's minimum. As was seen in the previous 2-D contour plots, a few of them did not have this property and so the range that this algorithm could be applied to had to be made smaller to cut out the "problem regions" where the metrics showed local maxima.

Below in Table~\ref{table:resultspart2} summarizing the results from using these more efficient minimizing algorithms.
\begin{table}[h]
\centering{
\begin{tabular}{|c|c|c|c|c|}\hline
\(\mathbf{\chi^2}\) & \(p_0^2\) & \(p_1\) & \(p_2\) & \(p_3\)\\\hline
1 & 2.84(+0.41,-0.38) & fixed & fixed & fixed\\\hline
2 & fixed & 27.09(+1.69,-1.6) & fixed & fixed\\\hline
3 & fixed & fixed & 21.7(+0.32,-0.31) & fixed\\\hline
4 & fixed & fixed & fixed & 5.21(+0.24,-0.22)\\\hline
5 &  2.6(+0.5,-0.4) & 26.9(+1.7,-1.7) & fixed & fixed\\\hline
6 & fixed & 26.8(+1.7,-1.7) & 21.7(+0.4,-0.4) & fixed\\\hline
7 & fixed & 25.3(+1.6,-1.5) & fixed & 5.2(+0.3,-0.3)\\\hline
8 & fixed & fixed & 21.7(+0.4,-0.4) & 5.0(+0.3,-0.3)\\\hline
\end{tabular}
}
\centering{
\begin{tabular}{|c|c|c|c|c|}\hline
\(\mathbf{-\log(L)}\) & \(p_0^2\) & \(p_1\) & \(p_2\) & \(p_3\)\\\hline
1 & 1.1(+0.31,-0.29) & fixed & fixed & fixed\\\hline
2 & fixed & 22.04(+1.53,-1.5) & fixed & fixed\\\hline
3 & fixed & fixed & 22.07(+0.31,-0.31) & fixed\\\hline
4 & fixed & fixed & fixed & 4.07(+0.2,-0.2)\\\hline
5 & 1.1(+0.4,-0.3) & 24.7(+1.6,-1.6) & fixed & fixed\\\hline
6 & fixed & 23.5(+1.6,-1.6) & 22.1(+0.3,-0.4) & fixed\\\hline
7 & fixed & 26(+1.8,-1.8) & fixed & 4.0(+0.2,-0.2)\\\hline
8 & fixed & fixed & 22.0(+0.4,-0.4) & 4.2(+0.3,-0.2)\\\hline
\end{tabular}
}
\caption[]{Results for the minimal values of \(\chi^2\) and \(-\log(L)\) and their respective uncertainties given by where the metric value changes from it's minimum by 1/2. These results were calculated using the more efficient minimization algorithms implemented in the second part of this lab. Note that we get the same exact results as in the first part of the lab, which is expected of course since the functions we are finding the exact minima for remained the same, and this tells us that the code is working properly as far as we can tell which is good.}
\label{table:resultspart2}
\end{table}

\subsection{Using the ROOT Fit Panel}
We also tested these same fits using the ROOT fit panel. It was pretty straight forward to use, and some fits worked fine with no problem, while others were not reliable. Alternatively, when the \(\chi^2\) fit wouldn
t converge, and then the likelihood fit was applied it would give a good fit, and then going back to the \(\chi^2\) fit with an already reasonable value in place causes the \(\chi^2\) fit to then work. This behavior is indicative that performing fits this way is probably not the most reliable or ideal way to do a real analysis unless you already have a pretty good idea of what the best fit would be and just want to find a more accurate value in a limited range. See the below figures to see the results of testing with the fit panel, with all of the results contained in the statistics legend of the plots.

\begin{figure}[htb]
\subfigure[A \(\chi^2\) fit using the ROOT fit panel, with the floating parameter range set too broad for ROOT to converge on a reasonable fit.]{
\centering{
\includegraphics[width=0.95\textwidth,keepaspectratio]{figures/fitpanel_bad.pdf}
}
}
\subfigure[A likelihood fit using the ROOT fit panel, with the floating parameter range set broad, yet ROOT can still converge on a reasonable fit using this method.]{
\centering{
\includegraphics[width=0.95\textwidth,keepaspectratio]{figures/fitpanel_badlikeli.pdf}
}
}
\caption{This is an example of where leaving the fit parameter floating over too broad of a range with the ROOT fit panel will simply cause the fit to fail. Notably, this only occurs for the \(\chi^2\) fits (a), as we can see that when applying a likelihood fit (b) it converges to a reasonable result over the same broad range.}
\label{fig:p0_1d_fitpanelbadexample}
\end{figure}

\begin{figure}[htb]
\subfigure[A \(\chi^2\) fit using ROOT fit panel.]{
\centering{
\includegraphics[width=0.95\textwidth,keepaspectratio]{figures/p0_1d_fitpanel.pdf}
}
}
\subfigure[A likelihood fit using ROOT fit panel.]{
\centering{
\includegraphics[width=0.95\textwidth,keepaspectratio]{figures/p0_1d_fitpanel_likeli.pdf}
}
}
\caption{A \(\chi^2\) and likelihood fit using ROOT fit panel, with one parameter floating.}
\label{fig:p0_1d_fitpanel}
\end{figure}

\begin{figure}[htb]
\subfigure[A \(\chi^2\) fit using ROOT fit panel.]{
\centering{
\includegraphics[width=0.95\textwidth,keepaspectratio]{figures/p0_1d_fitpanel.pdf}
}
}
\subfigure[A likelihood fit using ROOT fit panel.]{
\centering{
\includegraphics[width=0.95\textwidth,keepaspectratio]{figures/p0_1d_fitpanel_likeli.pdf}
}
}
\caption{A \(\chi^2\) and likelihood fit using ROOT fit panel, with one parameter floating.}
\label{fig:p0_1d_fitpanel}
\end{figure}

\begin{figure}[htb]
\subfigure[A \(\chi^2\) fit using ROOT fit panel.]{
\centering{
\includegraphics[width=0.95\textwidth,keepaspectratio]{figures/p1_1d_fitpanel.pdf}
}
}
\subfigure[A likelihood fit using ROOT fit panel.]{
\centering{
\includegraphics[width=0.95\textwidth,keepaspectratio]{figures/p1_1d_fitpanel_likeli.pdf}
}
}
\caption{A \(\chi^2\) and likelihood fit using ROOT fit panel, with one parameter floating.}
\label{fig:p1_1d_fitpanel}
\end{figure}

\begin{figure}[htb]
\subfigure[A \(\chi^2\) fit using ROOT fit panel.]{
\centering{
\includegraphics[width=0.95\textwidth,keepaspectratio]{figures/p2_1d_fitpanel.pdf}
}
}
\subfigure[A likelihood fit using ROOT fit panel.]{
\centering{
\includegraphics[width=0.95\textwidth,keepaspectratio]{figures/p2_1d_fitpanel_likeli.pdf}
}
}
\caption{A \(\chi^2\) and likelihood fit using ROOT fit panel, with one parameter floating.}
\label{fig:p2_1d_fitpanel}
\end{figure}

\begin{figure}[htb]
\subfigure[A \(\chi^2\) fit using ROOT fit panel.]{
\centering{
\includegraphics[width=0.95\textwidth,keepaspectratio]{figures/p3_1d_fitpanel.pdf}
}
}
\subfigure[A likelihood fit using ROOT fit panel.]{
\centering{
\includegraphics[width=0.95\textwidth,keepaspectratio]{figures/p3_1d_fitpanel_likeli.pdf}
}
}
\caption{A \(\chi^2\) and likelihood fit using ROOT fit panel, with one parameter floating.}
\label{fig:p3_1d_fitpanel}
\end{figure}

\begin{figure}[htb]
\subfigure[A \(\chi^2\) fit using ROOT fit panel.]{
\centering{
\includegraphics[width=0.95\textwidth,keepaspectratio]{figures/p0p1_2d_fitpanel.pdf}
}
}
\subfigure[A likelihood fit using ROOT fit panel.]{
\centering{
\includegraphics[width=0.95\textwidth,keepaspectratio]{figures/p0p1_2d_fitpanel_likeli.pdf}
}
}
\caption{A \(\chi^2\) and likelihood fit using ROOT fit panel, with two parameters floating.}
\label{fig:p0p1_2d_fitpanel}
\end{figure}

\clearpage

\subsection{Overall Conclusions}
When calculating the log likelihood, you are limited to fitting with functions that are always positive. This is because the Poisson function will return 0 for any points where the function is negative, and taking the log of 0 is obviously problematic, especially computationally as a negative infinity value will propagate through your calculations, and lead to errors in your calculation of the log likelihood.

We can see from the results of this lab that the \(\chi^2\) and likelihood fits yield slightly different results for the data provided. When the value of \(f\) is sufficiently large the two methods should yield the same results, however that is clearly not the case for some of the points at which we sample our function to do our calculations. The value of \(\sigma\) in the denominator of the \(\chi^2\) function is therefore not completely accurate at these small values of \(f\), which can cause the deviations between the two fitting methods that we observed in this lab. Additionally, none of these fitting algorithms required heavy computational resources. They all completed in seconds (or fractions of a second). The 2 dimensional fits did take relatively significantly longer, however. Making the step size between parameter values to test an order of magnitude larger in the 2-D case (which reduces precision on our measurement), made the CPU time increase from about half of a second to about 20 seconds. We also see that using the more efficient algorithms provide the same minimization result, which helps us trust the algorithms we wrote, but it's important to keep in mind that this is only guaranteed for functions that only have one minimum critical point in the range that it is told to look in. If we were to use a function that had a global minimum and a local minimum elsewhere, it is possible to find the local minimum and report that as the global minimal result when that isn't the true value we are after.

Using the ROOT fit panel was easy and user-friendly, but only seemed to prove most useful and reliable if we already have an idea of what parameter values we want to fit the data with, though the likelihood fit is far more robust and converges even when the \(\chi^2\) fit wouldn't given the same parameter ranges to use. Additionally, it is also interesting to note the behavior of the \(\chi^2\) function when we allow the parameter \(p_0^2\) to be negative (see Fig.~\ref{fig:p0_neg}). We don't gain anything from actually allowing it to be negative, as it is a squared quantity, and so for an actual study where we were truly interested in a function where the parameter we are looking for is squared, we would want to limit the range of that parameter to be greater than 0.

\begin{figure}[htb]
\centering{
\includegraphics[width=0.75\textwidth,keepaspectratio]{figures/p0_neg.png}
}
\caption{The behavior of \(\chi^2\) when we allow the \(p_0^2\) parameter in the function to range over negative values. This wouldn't add any physical value to a problem, but it is an interesting behavior that the function takes on.}
\label{fig:p0_neg}
\end{figure}

\clearpage

\section*{Acknowledgments}
I would like to note that I used the ROOT software for this lab \cite{ROOT}.

\bibliography{refs}{}
\bibliographystyle{plain}

\end{document}